.. Trusted Substrate documentation master file, created by
   sphinx-quickstart on Wed May 25 16:19:39 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

########
Firmware
########

.. toctree::
    :maxdepth: 2
   
    general/index
    requirements/index
    building/index
    running/index
    references
    conventions
